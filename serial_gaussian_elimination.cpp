#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <cmath>
#include <stdio.h>
#include <string.h>
using namespace std;
//define DEBUG

void pivot(double** matrix, double* vector, int i, int n);
void print(string title, double** matrix, int n);
void print(string title, double* vector, int n);
void annihilate(double* diag_vector, double* annihilate_vector, double multiplier, int n);
double l_square_norm(double** matrix, double* b, double* x, int n);

int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        cout << "Invalid number of arguments. Giving up...\n";
        return(1);
    }

    int n = atoi(argv[1]);
    double** matrix = new double*[n];
    double* b = new double[n];
    double* x = new double[n];
    const double RANDMAX = 10E10;
    const double RANDMIN = -10E10;
    double val;
    double multiplier;
    double l_square_norm_result;

    /* Generate the random matrix and vector b */
    for (int i = 0; i < n; i++)
    {
        matrix[i] = new double[n];
        b[i] = drand48() * (RANDMAX - RANDMIN) + RANDMIN;
        
        /* generate matrix */
        for (int j = 0; j < n; j++)
        {
            //cin >> val;
            
            matrix[i][j] = drand48() * (RANDMAX - RANDMIN) + RANDMIN;
        }

        /* generate vector b */
    }

    /*for (int i = 0; i < n; i++)
    {
        cin >> b[i];
    }*/

    #ifdef DEBUG
        print("Original Matrix", matrix, n);
        print("Vector B", b, n);
    #endif

    /* perform forward elimination */
    for (int i = 0; i < n-1; i++)
    {
        /* at the start of each new row, check for pivoting */
        pivot(matrix, b, i, n);
        
        #ifdef DEBUG
        print("pivoted", matrix, n);
        #endif

        for (int j = i+1; j < n; j++)
        {
            #ifdef DEBUG
                cout << "row = " << j << endl;
                cout << "col = " << i << endl;
            #endif
            
            multiplier = matrix[j][i]/matrix[i][i];
            
            #ifdef DEBUG
                cout << "multiplier = " << multiplier << endl;
            #endif

            for (int k = i; k < n; k++)
            {
                matrix[j][k] -= (multiplier * matrix[i][k]);
            }
            b[j] = b[j] - multiplier * b[i];
            
            #ifdef DEBUG
                print("matrix After Annihilation", matrix, n);
                print("B after Annihilation", b, n);
            #endif
        }
    }

    /* perform back substitution */
    x[n-1] = b[n-1] / matrix[n-1][n-1];
    for (int i = n-2; i >= 0; i--)
    {
        double temp = b[i];
        for (int j = i+1; j < n; j++)
        {
            temp = temp - matrix[i][j] * x[j];
        }
        x[i] = temp / matrix[i][i];

    }
    
    #ifdef DEBUG
        print("Vector X", x, n);
    #endif

    /* Check for correct answer by calculating l^2-Norm */
    l_square_norm_result = l_square_norm(matrix, b, x, n);
    cout << "l^2-Norm Result = " << l_square_norm_result << endl;
    

    return 0;
}

double l_square_norm(double** matrix, double* b, double* x, int n)
{
    /* allocate a temporary vectors to store the result of Ax - b */
    double* tmp_ax = new double[n];
    double* tmp_result = new double[n];
    double result = 0.0;

    for (int i = 0; i < n; i++)
        tmp_ax[i] = 0;
    
    /* multiply A*x */
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            tmp_ax[i] += matrix[i][j] * x[j];
        }
    }

    /* tmp stores result of A*x. Now subtract b from Ax and store in result */
    for (int i = 0; i < n; i++)
    {
        #ifdef DEBUG
            cout << "Ax[" << i << "] = " << tmp_ax[i] << endl
                 << "b[" << i << "] = " << b[i] << endl;
        #endif
                 
        result += pow((tmp_ax[i] - b[i]), 2);
    }

    delete tmp_ax;
    delete tmp_result;

    return sqrt(result);
}

void print(string title, double* vector, int n)
{
    cout << "----- " << title << " -----" << endl << "[";
    
    for(int i =0; i < n; i++)
    {
        cout <<  vector[i];
        if (i != n-1)
            cout << " ";
    }
    cout << "]\n\n";
}

void pivot(double** matrix, double* vector, int i, int n)
{
    /* starting at row i, find the largest abs value in the column 
       and swap that row with the starting row i */
    int max_index = i;
    double max_abs_value = abs(matrix[i][i]);

    for (int row = i+1; row < n; row++)
    {
        if (abs(matrix[row][i]) > max_abs_value)
        {
            max_abs_value = abs(matrix[row][i]);
            max_index = row;
        }
    }
    
    /* if the max_index is not the row we started with, then we need to swap
     * the rows of the matrix and the elements of the vector*/
    if (i != max_index)
    {
        /* swap rows of the matrix */
        double *tmp_ptr = matrix[i];
        matrix[i] = matrix[max_index];
        matrix[max_index] = tmp_ptr;

        /* swap elements of the vector */
        double tmp = vector[i];
        vector[i] = vector[max_index];
        vector[max_index] = tmp;

    }

}

void print(string title, double** matrix, int n)
{
    cout << "----- " << title << " -----\n";

    /* print the matrix */
    for (int i = 0; i < n; i++)
    {
        cout << "[";
        for (int j = 0; j < n; j++)
        {
            cout << matrix[i][j];
            if (j != n-1)
                cout << "\t";
        }
        cout << "]" << endl;
    }
    cout << endl;
}












