/* File:    gaussian_elimination.c
 * Purpose: Solve a system of equations using Gaussian Eliminate with
 *          partial pivoting.
 *
 * Input:   n, the size of the square matrix A
 *          p, the number of threads to run the program with. 
 *
 * Output:  solve the system of equations an output the elapsed time and
 *          the l^2-Norm error
 *
 * Compile: gcc -g -Wall -fopenmp -o gaussian_elimination gaussian_elimination.c -lm
 * Usage:   ./omp_trap2b <n> <p>
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <string.h>

#define RANDMAX 1.0E10
#define RANDMIN -1.0E10

//#define DEBUG

void pivot(double** matrix, double* vector, int i, int n);
void print_matrix(char title[], double** matrix, int n);
void print_vector(char title[], double* vector, int n);
void generate(double** a, double* b, int n);
double l_square_norm(double** matrix, double* b, double* x, int n);

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        printf("Usage: ./gaussian_elimination <n> <p>\n");
        return 1;
    }
    
    int         n = atoi(argv[1]);
    int         thread_count = atoi(argv[2]);
    double**    A = malloc(sizeof(double*) * n);
    double*     x = malloc(sizeof(double) * n);
    double*     b = malloc(sizeof(double) * n);
    double      multiplier;
    double      l_square_norm_result;
    int         i, j, k;
    double      start_time, finish_time;

    generate(A, b, n); // initialize matrix A and vector b with random values
    
    #ifdef DEBUG
        print_matrix("Original Matrix A", A, n);
        print_vector("Original Vector b", b, n);
    #endif
    
    if (thread_count == 1)
        printf("Running 1 thread\nTotal available cores: %d\n", omp_get_num_procs());
    else
        printf("Running %d threads\nTotal available cores: %d\n", thread_count, omp_get_num_procs());

    /* begin timing */
    start_time = omp_get_wtime();

       /* perform forward elimination */
    for (i = 0; i < n-1; i++)
    {
        /* at the start of each new row, check for pivoting */
        pivot(A, b, i, n);
        
        #ifdef DEBUG
        print_matrix("pivoted", A, n);
        #endif
       
        #pragma omp parallel for default(none) schedule(static) \
        num_threads(thread_count) shared(A, b, n, i) private(j, k, multiplier) 
            for (j = i+1; j < n; j++)
            {
                #ifdef DEBUG
                    printf("row = %d", j);
                    printf("col = %d", i);
                #endif
                
                multiplier = A[j][i]/A[i][i];
                
                #ifdef DEBUG
                    printf("multiplier = %lf", multiplier);
                #endif
        
                for (k = i; k < n; k++)
                {
                    A[j][k] -= (multiplier * A[i][k]);
                }
                b[j] = b[j] - multiplier * b[i];
                
                #ifdef DEBUG
                    print_matrix("matrix After Annihilation", A, n);
                    print_vector("B after Annihilation", b, n);
                #endif
            }
        
    }

    /* perform back substitution */

    /* start at the last row and find x[n-1] because there is only one unknown */
    x[n-1] = b[n-1] / A[n-1][n-1];

    /* then move up row by row solving for each x */
    for (i = n-2; i >= 0; i--)
    {
        double temp = b[i];
        for (j = i+1; j < n; j++)
        {
            temp = temp - A[i][j] * x[j];
        }
        x[i] = temp / A[i][i];

    }

    /* end of timing */
    finish_time = omp_get_wtime();
    printf("elapsed time = %lf seconds\n", finish_time - start_time);
    
    #ifdef DEBUG
        print_vector("Vector X", x, n);
    #endif

    /* Check for correct answer by calculating l^2-Norm */
    l_square_norm_result = l_square_norm(A, b, x, n);
    printf("l^2-Norm Result = %lf\n", l_square_norm_result);

    return 0;
} /* end of main */

/*-------------------------------------------------------------------
 * Function:  pivot
 * Purpose:   Check if any of the rows below the diagonal should be swapped 
 *            with the row on the diagonal. If so, swap the rows. 
 *
 * In args:   matrix:   matrix whose rows will be swapped if necessary
 *            vector:   vector whose element at the same row as the matrix
 *                      will also be swapped if necessary
 *            i         the row of the current diagonal element.
 *            n         global number of elements
 *
 * Notes:   The function doesn't copy every element to swap the rows, it
 *          simply swaps the pointers to that row.
 */

void pivot(double** matrix, double* vector, int i, int n)
{
    /* starting at row i, find the largest abs value in the column 
       and swap that row with the starting row i */
    int max_index = i;
    double max_abs_value = fabs(matrix[i][i]);
    int row;

    for (row = i+1; row < n; row++)
    {
        if (fabs(matrix[row][i]) > max_abs_value)
        {
            max_abs_value = fabs(matrix[row][i]);
            max_index = row;
        }
    }
    
    /* if the max_index is not the row we started with, then we need to swap
     * the rows of the matrix and the elements of the vector*/
    if (i != max_index)
    {
        /* swap rows of the matrix */
        double *tmp_ptr = matrix[i];
        matrix[i] = matrix[max_index];
        matrix[max_index] = tmp_ptr;

        /* swap elements of the vector */
        double tmp = vector[i];
        vector[i] = vector[max_index];
        vector[max_index] = tmp;

    }

}

/*-------------------------------------------------------------------
 * Function:  generate
 * Purpose:   Generate a pseudo-random matrix of n*n elements and a 
 *            vector of n elements
 *
 * In args:   n:    global number of components
 *
 * Out args:  a:    a random matrix of size n*n
 *            b:    a random vector of size n
 *
 * Notes:   Assumes pre-processor constants RANDMAX and RANDMIN are defined
 */
void generate(double** a, double* b, int n)
{
    int i, j;

    for (i = 0; i < n; i++)
    {
        a[i] = malloc(sizeof(double) * n);

        /* generate random values for vector b */
        b[i] = drand48() * (RANDMAX - RANDMIN) + RANDMIN;

        /* generate random values for matrix A */
        for (j = 0; j < n; j++)
        {
            a[i][j] = drand48() * (RANDMAX - RANDMIN) + RANDMIN;
        }
    }
} /* end of generate */

/*-------------------------------------------------------------------
 * Function:  l_square_norm
 * Purpose:   calculates the l^2 Norm value 
 * In args:   matrix:   A matrix 
 *            b:        A vector 
 *            n:        number of components
 *
 * Returns:  the square root of the sum of the elements squared of the 
 *           vector resulting from the operation Ax-b
 */

double l_square_norm(double** matrix, double* b, double* x, int n)
{
    int i, j;

   /* allocate a temporary vector to store the result of Ax */
    double* tmp_ax = malloc(sizeof(double) * n);
    double result = 0.0;

    for (i = 0; i < n; i++)
        tmp_ax[i] = 0;
    
    /* multiply A*x */
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            tmp_ax[i] += matrix[i][j] * x[j];
        }
    }

    /* tmp stores result of A*x. Now subtract b from Ax and store in result */
    for (i = 0; i < n; i++)
    {
        #ifdef DEBUG
            printf("Ax[%d] = %lf\n", i, tmp_ax[i]);
            printf( "b[%d] = %lf\n", i, b[i]);
        #endif
                 
        result += pow((tmp_ax[i] - b[i]), 2);
    }

    free(tmp_ax);

    return sqrt(result);

} /* end of l_square_norm */

/*-------------------------------------------------------------------
 * Function:  Print
 * Purpose:   Print a matrix
 * In args:   title:      name of matrix
 *            matrix      matrix to be printed
 *            n:          global number of components
 */
void print_matrix(char title[], double** matrix, int n)
{
    int row, col;

    printf("\n----- %s -----\n", title);

    for (row = 0; row < n; row++)
    {
        printf("[");
        for (col = 0; col < n; col++)
        {
            printf("%lf", matrix[row][col]);
            if (col != n-1)
                printf("\t");
        }

        printf("]\n");
    }

    printf("\n\n");
} /* end of Print */

/*-------------------------------------------------------------------
 * Function:  Print
 * Purpose:   Print a vector
 * In args:   title:      name of vector
 *            vector:     vector to be printed
 *            n:          global number of components
 */
void print_vector(char title[], double* vector, int n)
{
    int row;

    printf("\n----- %s -----\n[", title);

    for (row = 0; row < n; row++)
    {
            printf("%lf", vector[row]);
            if (row != n-1)
                printf(" ");

    }

    printf("]\n\n");
} /* end of Print */





